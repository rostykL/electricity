/**
 * This class is just a facade for your implementation, the tests below are using the `World` class only.
 * Feel free to add the data and behavior, but don't change the public interface.
 */

export class World {
  constructor() {
    // PowerPlants
    this.powerPlants = {
      available: {},
      killed: [],
    };
    // Households
    this.households = {
      connectedHouseHolds: {},
      available: {},
    };

    //  Utils
    this.powerPlantId = 0;
    this.householdId = 0;
  }

  createPowerPlant() {
    const pp = {
      id: ++this.powerPlantId,
      name: `PowerPlant ${this.powerPlantId}`,
      killed: false,
      host: [],
    };
    if (!this.powerPlants.available[pp.name]) {
      this.powerPlants.available[pp.name] = pp;
    }
    return this.powerPlants.available[pp.name];
  }

  createHousehold() {
    const hh = {
      id: ++this.householdId,
      name: `Household ${this.householdId}`,
      provider: false,
      hasElectricity: false,
    };

    if (!this.households.available[hh.name]) {
      this.households.available[hh.name] = hh;
    }
    return this.households.available[hh.name];
  }

  connectHouseholdToPowerPlant(household, powerPlant) {
    // return from the function if the powerPlant is killed
    const isPowerPlantKilled = this.powerPlants.killed.includes(
      powerPlant.name
    );
    if (isPowerPlantKilled) return;

    // get all connected households to given household
    const connectedHousehold =
      this.households.connectedHouseHolds[household.name] || [];

    // get all connected households to given household except the given household
    const findConnectionForThisHousehold = connectedHousehold.filter(
      (el) => el.id !== household.id
    );

    findConnectionForThisHousehold.forEach((el) => {
      const hhConnection = !!this.households.connectedHouseHolds[el?.name];
      if (hhConnection) {
        const filter = this.households.connectedHouseHolds[el?.name].filter(
          (ell) => ell.id !== el.id
        );
        connectedHousehold.push(...filter);
      }
    });

    this.powerPlants.available[powerPlant.name].host.push(
      household,
      ...connectedHousehold
    );

    // set hasElectricity for all households with elec to true
    this.powerPlants.available[powerPlant.name].host.forEach((el) => {
      this.households.available[el.name] = {
        ...this.households.available[el.name],
        hasElectricity: true,
      };
    });
  }

  connectHouseholdToHousehold(household1, household2) {
    // check if we households are the same
    if (household1.name === household2.name) return;

    // check if we already have connected households to household1
    const connectedHousehold =
      this.households.connectedHouseHolds[household1.name] || [];

    // find household1 in array and set provider to true
    this.households.available[household1.name] = {
      ...this.households.available[household1.name],
      provider: true,
    };

    const thisHousehold = this.households.available[household2.name];

    // add newHousehold to already connected
    connectedHousehold.push(thisHousehold);

    this.households.connectedHouseHolds[household1.name] = [
      ...connectedHousehold,
    ];
  }

  disconnectHouseholdFromPowerPlant(householdP, powerPlant) {
    const saveHost = this.powerPlants.available[powerPlant.name].host;

    this.powerPlants.available[powerPlant.name].host = [];

    // set electricity to true for all households that have it
    saveHost.forEach((el) => {
      this.households.available[el.name] = {
        ...this.households.available[el.name],
        hasElectricity: false,
      };
    });
  }

  killPowerPlant(powerPlant) {
    const saveHost = this.powerPlants.available[powerPlant.name].host;

    // set killed to true for the certain powerPlant
    this.powerPlants.available[powerPlant.name] = {
      ...this.powerPlants.available[powerPlant.name],
      killed: true,
    };
    this.powerPlants.killed.push(powerPlant.name);

    saveHost.forEach((el) => {
      this.households.available[el.name] = {
        ...this.households.available[el.name],
        hasElectricity: false,
      };
    });
  }

  repairPowerPlant(powerPlant) {
    const tmp = this.powerPlants.available[powerPlant.name].host;
    tmp.forEach((el) => {
      this.households.available[el.name] = {
        ...this.households.available[el.name],
        hasElectricity: true,
      };
    });

    this.powerPlants.available[powerPlant.name] = {
      ...this.powerPlants.available[powerPlant.name],
      killed: false,
    };

    //
    this.powerPlants.killed = this.powerPlants.killed.filter(
      (el) => el !== powerPlant.name
    );
  }

  householdHasEletricity(household) {
    return this.households.available[household.name].hasElectricity;
  }
}

const world = new World();

const h1 = world.createHousehold();
const h2 = world.createHousehold();
const h3 = world.createHousehold();

const p1 = world.createPowerPlant();
const p2 = world.createPowerPlant();

world.connectHouseholdToHousehold(h1, h2);
world.connectHouseholdToHousehold(h1, h1);

world.connectHouseholdToHousehold(h2, h3);

world.connectHouseholdToPowerPlant(h1, p1);

world.connectHouseholdToPowerPlant(h2, p2);

world.connectHouseholdToHousehold(h2, h1);

world.disconnectHouseholdFromPowerPlant(h2, p2);

// world.killPowerPlant(p1);
// world.repairPowerPlant(p1);
// world.disconnectHouseholdFromPowerPlant(h1, p1);
console.dir(world, { depth: null });
